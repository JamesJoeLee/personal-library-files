# 该程序为读取yaml文件的通用程序-采用ruamel.yaml库
from ruamel.yaml import YAML


# 通过KEY关键字更新其值
# ·yamlFile:<string> yaml文件的地址
# ·encodingFormat:<string> yaml文件的解码格式
# ·updateKey:需要更新的关键字
# ·updateValue:更新的内容
# ·return： 正确返回true
#           异常返回False
def updateYamlByKey(yamlFile, updateKey, updateValue, encodingFormat):
    try:
        with open(yamlFile, "r", encoding=encodingFormat) as yrFile:
            yamlData = YAML().load(yrFile)
            yamlData[updateKey] = updateValue

        with open(yamlFile, "w", encoding=encodingFormat) as ywFile:
            YAML().dump(yamlData, ywFile)
        return True
    except Exception as e:
        print("错误:" + str(e))
        return False
    finally:
        yrFile.close()
        ywFile.close()


# 创建一个新的yaml文件并将数据写入
def writeYaml(yamlTxt, yamlname, yamlFilePath, encodingFormat):
    print()
    # 判断文件是否存在不存在就创建存在就报错
