# 该程序为读取yaml文件的通用程序-采用ruamel.yaml库

from ruamel.yaml import YAML


# 读取yaml文件中所有项目
# ·yamlFile:<string> yaml文件的地址
# ·encodingFormat:<string> yaml文件的解码格式
# ·return： 正确返回yamlData数据，类型：<class 'ruamel.yaml.comments.CommentedMap'>
#           异常返回False
def readAllYaml(yamlFile, encodingFormat):
    try:
        with open(yamlFile, "r", encoding=encodingFormat) as yFile:
            yamlData = YAML().load(yFile)
        return yamlData
    except Exception as e:
        print('错误：' + str(e))
        return False
    finally:
        yFile.close()


# 根据关键字读取配置文件
# ·yamlFile:<string> yaml文件的地址
# ·encodingFormat:<string> yaml文件的解码格式
# key:<string> 读取的关键字
# ·return： 正确返回关键字的数据  类型：<class 'str'>
#           异常返回False
def readYamlByKey(yamlFile, key, encodingFormat):
    try:
        with open(yamlFile, "r", encoding=encodingFormat) as yFile:
            yamlData = YAML.load(yFile)
        return yamlData[key]
    except Exception as e:
        print('错误：' + str(e))
        return False
    finally:
        yFile.close()
